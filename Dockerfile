FROM alpine:3.14 as base
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY index.html ./
RUN echo LOLZ >> /root/index.html

FROM httpd:alpine
WORKDIR /usr/local/apache2/htdocs/
COPY --from=base /root/index.html ./
 
 
